#!/usr/bin/env python

import os
import paramiko 

ssh = paramiko.SSHClient()
ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
# enter server details here
ssh.connect("ip-address", username="root", password="password")

# upload the firmware
sftp = ssh.open_sftp()
localpath = '/home/aldin-sxr/Arduino/flink-device-core/.pioenvs/nodemcuv2/firmware.bin'
remotepath = '/var/www/html/new/bin/cool-code.bin'
sftp.put(localpath, remotepath)
sftp.close()
print("Successfully uploaded the firmware to remote server.")

# upload the SPIFFS
sftp = ssh.open_sftp()
localpath = '/home/aldin-sxr/Arduino/flink-cool-electronics/.pioenvs/nodemcuv2/spiffs.bin'
remotepath = '/var/www/html/new/bin/cool-spiffs.bin'
sftp.put(localpath, remotepath)
sftp.close()
print("Successfully uploaded the SPIFFS to remote server.")

ssh.close()