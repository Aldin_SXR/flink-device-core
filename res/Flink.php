<?php
/* PHP script for sending the initial POST to device in order to establish MQTT communication */
class Flink {
    public function __construct() {
        $this->username = "Aldin-SXR";
        $this->password = "random-test";
        $this->uuid = "4ed5dab1-c190-4a42-a1ff-e574fb707388";
    }

    /* Post a UUID message */
    public function post_uuid($u) {
        $data = array(
            "Username" => $this->username,
            "Password" => $this->password,
            "Uuid" => $this->uuid
        );
        $url = "http://$u/config";
        $ch = curl_init($url);
        $json_data = json_encode($data);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $json_data);
        curl_setopt($ch, CURLOPT_HEADER, array("Content-Type: application/json"));
        $result = curl_exec($ch);
    }
}

$flink = new Flink();
$flink->post_uuid("192.168.88.151"); // specify IP address of the device
?>