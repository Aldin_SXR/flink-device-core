/**
 * 
 * @file EString.h
 * @date 19.12.2017
 * @author Aldin Kovačević
 * 
 * Library for writing/reading strings into EEPROM
 * 
 * */

#ifndef ESTRING_H
#define ESTRING_H 
#include <Arduino.h>
#include <EEPROM.h>
#include <WString.h>

class EString {
    public:
        boolean writeString(int address, String input);
        String readString(int address, int length);
        void writeByte(int address, int value);
        int readByte(int address);
    protected:
        boolean eeprom_is_addr_ok(int address);
        boolean eeprom_write_bytes(int startAddr, const byte* array, int numBytes) ;
        boolean eeprom_write_string(int addr, const char* string);
        boolean eeprom_read_string(int addr, char* buffer, int bufSize);
};

extern EString eeprom;

#endif