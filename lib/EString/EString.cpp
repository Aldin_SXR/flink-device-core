/**
 * 
 * @file EString.cpp
 * @date 19.12.2017
 * @author Aldin Kovačević
 * 
 * Library for writing/reading strings into EEPROM
 * 
 * */
#include "EString.h"

boolean EString::eeprom_is_addr_ok(int addr) {
  const int EEPROM_MIN_ADDR = 0;
  const int EEPROM_MAX_ADDR = 511;
  return ((addr >= EEPROM_MIN_ADDR) && (addr <= EEPROM_MAX_ADDR));
}

boolean EString::writeString(int Addr, String input) {
    char cbuff[input.length()+1]; //Finds length of string to make a buffer
    input.toCharArray(cbuff,input.length()+1); //Converts String into character array
    EEPROM.begin(512);
    boolean result = eeprom_write_string(Addr,cbuff); //Saves String 
    EEPROM.end();
    return result;
}

String EString::readString(int Addr, int length) {
  char cbuff[length+1];
  EEPROM.begin(512);
  eeprom_read_string(Addr, cbuff, length+1);
  EEPROM.end();
  String stemp(cbuff);
  return stemp;
}

boolean EString::eeprom_write_bytes(int startAddr, const byte* array, int numBytes) {
  // counter
  int i;
  // both first byte and last byte addresses must fall within
  // the allowed range
  if (!eeprom_is_addr_ok(startAddr) || !eeprom_is_addr_ok(startAddr + numBytes)) {
    return false;
  }

  for (i = 0; i < numBytes; i++) {
    EEPROM.write(startAddr + i, array[i]);
  }
 
  return true;
}
 
boolean EString::eeprom_write_string(int addr, const char* string) {
  int numBytes; // actual number of bytes to be written
  //write the string contents plus the string terminator byte (0x00)
  numBytes = strlen(string) + 1;
  return eeprom_write_bytes(addr, (const byte*)string, numBytes);
}
 
boolean EString::eeprom_read_string(int addr, char* buffer, int bufSize) {
  const int EEPROM_MIN_ADDR = 0;
  const int EEPROM_MAX_ADDR = 511;
  byte ch; // byte read from eeprom
  int bytesRead; // number of bytes read so far
 
  if (!eeprom_is_addr_ok(addr)) { // check start address
    return false;
  }
 
  if (bufSize == 0) { // how can we store bytes in an empty buffer ?
    return false;
  }
 
  if (bufSize == 1) {
    buffer[0] = 0;
    return true;
  }
 
  bytesRead = 0; // initialize byte counter
  ch = EEPROM.read(addr + bytesRead); // read next byte from eeprom
  buffer[bytesRead] = ch; // store it into the user buffer
  bytesRead++; // increment byte counter

  while ( (ch != 0x00) && (bytesRead < bufSize) && ((addr + bytesRead) <= EEPROM_MAX_ADDR) ) {
    // if no stop condition is met, read the next byte from eeprom
    ch = EEPROM.read(addr + bytesRead);
    buffer[bytesRead] = ch; // store it into the user buffer
    bytesRead++; // increment byte counter
  }
  if ((ch != 0x00) && (bytesRead >= 1)) {
    buffer[bytesRead - 1] = 0;
  }
 
  return true;
}

void EString::writeByte(int address, int value) {
  EEPROM.begin(512);
  EEPROM.write(address, value);
  EEPROM.end();
}

int EString::readByte(int address) {
  EEPROM.begin(512);
  int value = EEPROM.read(address);
  EEPROM.end();
  return value;
}

EString eeprom;