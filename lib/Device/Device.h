/**
 * 
 * @file Device.h
 * @date 09.12.2017
 * @author Aldin Kovačević
 * 
 * Device library containing all necessary includes, defines and 
 * global variables necessary for the proper functioning of "Flink" devices
 * 
 */

#ifndef DEVICE_H
#define DEVICE_H

 /* Necessary libraries */
 #include <Arduino.h>
#include <ESP8266WiFi.h> 
#include <ESP8266WebServer.h>
#include <SPI.h> // SPIFFS library
#include "EString.h" // EEPROM writing/reading
#include <WiFiClient.h> 
#include <ArduinoJson.h> // Arduino JSON handler library
#include <TaskScheduler.h> 
#include "FileStream.h" // SPIFFS writing/reading
#include "WString.h" // String library
#include "FlinkHTTPUpdate.h"// OTA code/SPIFFS update
#include "AsyncMqttClient.h" // Asynchronous MQTT library
#include "FlinkTime.h" // Clock & date library


/* Defines */
#define  FPSTR(pstr_pointer) (reinterpret_cast<const __FlashStringHelper *>(pstr_pointer))
#define F(string_literal) (FPSTR(PSTR(string_literal)))
#define DEBUG

/* Global variables */
ESP8266WebServer server(80);
IPAddress local(0, 0, 0, 0);
IPAddress MQTTServer (139, 59, 136, 102);
WiFiClient espClient;
AsyncMqttClient client;
Scheduler s_wifiConnection;

/* Task Scheduler mechanism */
void wifiConnect();
void enableSmartConfig();
void checkStatusSmartConfig();
void checkStatusWifiConnection();
void disableSmartConfig();
void mqttReconnect();
void setDefault();
void OTAupdate();
void OTASpiffsUpdate();
void resyncTime();
void actualReset();

Task t_wifiConnect(5000, TASK_FOREVER, &wifiConnect, &s_wifiConnection, false);
Task t_enableSmartConfig(30000, TASK_FOREVER, &enableSmartConfig, &s_wifiConnection, false);
Task t_checkStatusSmartConfig(500, TASK_FOREVER, &checkStatusSmartConfig, &s_wifiConnection, false);
Task t_checkStatusWifiConnection(1000, TASK_FOREVER, &checkStatusWifiConnection, &s_wifiConnection, false);
Task t_disableSmartConfig(30000, TASK_FOREVER, &disableSmartConfig, &s_wifiConnection, false);
Task t_mqttReconnect(10000, TASK_FOREVER, &mqttReconnect, &s_wifiConnection, false);
Task t_setDefault(1000, TASK_FOREVER, &setDefault, &s_wifiConnection, false);
Task t_OTAupdate(1000, TASK_FOREVER, &OTAupdate, &s_wifiConnection, false);
Task t_OTASpiffsUpdate(1000, TASK_FOREVER, &OTASpiffsUpdate, &s_wifiConnection, false);
Task t_resyncTime(1000, TASK_FOREVER, &resyncTime, &s_wifiConnection, false);
Task t_actualReset(2000, TASK_FOREVER, &actualReset, &s_wifiConnection, false);

#endif