/**
 * 
 * @file FlinkTime.h
 * @date 11.02.2018
 * @author Aldin Kovačević
 * 
 * Flink real-time clock library
 * 
 * */

#ifndef FLINK_TIME_H
#define FLINK_TIME_H
#include <Arduino.h>
#include <TimeLib.h>
#include <ESP8266WiFi.h>
#include <WiFiUdp.h>
#define TIME_DEBUG

time_t getNtpTime();

class FlinkTime {
    public:
        FlinkTime();
        ~FlinkTime();
        void timeSync(int zone = 1);
        void loop();
        void digitalClockDisplay();
        void printDigits(int digits);
        void sendNtpPacket(IPAddress &address);
};

extern  FlinkTime flinkClock;

#endif 
