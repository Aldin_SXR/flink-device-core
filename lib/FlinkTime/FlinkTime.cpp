/**
 * 
 * @file FlinkTime.cpp
 * @date 11.02.2018
 * @author Aldin Kovačević
 * 
 * Flink real-time clock library
 * 
 * */

#include "FlinkTime.h"

const int NTP_PACKET_SIZE = 48; // NTP time is in the first 48 bytes of message
byte flinkPacketBuffer[NTP_PACKET_SIZE]; //buffer to hold incoming & outgoing packets
static const char ntpServerName[] = "europe.pool.ntp.org"; // NTP server pool
WiFiUDP flinkUdp;
unsigned int localPort = 8888;  // local port to listen for UDP packets
time_t prevDisplay = 0; // when the digital clock was displayed
int timeZone;

FlinkTime::FlinkTime() {}
FlinkTime::~FlinkTime() {}

void FlinkTime::timeSync(int zone) {
    flinkUdp.begin(localPort);
    #ifdef TIME_DEBUG
    Serial.print("[TimeSync] Local port: ");
    Serial.println(flinkUdp.localPort());
    Serial.println("[TimeSync] Waiting for sync...");
    #endif
    timeZone = zone;
    setSyncProvider(getNtpTime);
    setSyncInterval(43200);
    #ifdef TIME_DEBUG
    digitalClockDisplay();
    #endif
}

void FlinkTime::loop() {
  // don't loop if WiFi isn't connected
  if ((WiFi.status() == WL_CONNECTED)) {
    if (timeStatus() != timeNotSet) {
        if (now() != prevDisplay)  //update the display only if time has changed
            prevDisplay = now();
     }
  }
}

void FlinkTime::digitalClockDisplay() {
    // digital clock display of the time
    #ifdef TIME_DEBUG
    Serial.println("<--- [Clock] Current time & date --->");
    Serial.print(hour());
    printDigits(minute());
    printDigits(second());
    Serial.print(" ");
    Serial.print(day());
    Serial.print(".");
    Serial.print(month());
    Serial.print(".");
    Serial.print(year());
    Serial.print(" [");
    Serial.print(dayStr(weekday()));
    Serial.print("]");
    Serial.println("\n");
    #endif
}

void FlinkTime::printDigits(int digits) {
  // utility for digital clock display: prints preceding colon and leading 0
  Serial.print(":");
  if (digits < 10)
    Serial.print('0');
  Serial.print(digits);
}

time_t getNtpTime() {
    IPAddress ntpServerIP; // NTP server's ip address
    while (flinkUdp.parsePacket() > 0) ; // discard any previously received packets
    #ifdef TIME_DEBUG
    Serial.println("[NTP] Transmiting NTP request...");
    #endif
    // get a random server from the pool
    WiFi.hostByName(ntpServerName, ntpServerIP);
    #ifdef TIME_DEBUG
    Serial.print("[NTP] ");
    Serial.print(ntpServerName);
    Serial.print(": ");
    Serial.println(ntpServerIP);
    #endif
    flinkClock.sendNtpPacket(ntpServerIP);
    uint32_t beginWait = millis();
    while (millis() - beginWait < 1500) {
        int size = flinkUdp.parsePacket();
        if (size >= NTP_PACKET_SIZE) {
            #ifdef TIME_DEBUG
            Serial.println("[NTP] Received NTP response");
            #endif
            flinkUdp.read(flinkPacketBuffer, NTP_PACKET_SIZE);  // read packet into the buffer
            unsigned long secsSince1900;
            // convert four bytes starting at location 40 to a long integer
            secsSince1900 =  (unsigned long)flinkPacketBuffer[40] << 24;
            secsSince1900 |= (unsigned long)flinkPacketBuffer[41] << 16;
            secsSince1900 |= (unsigned long)flinkPacketBuffer[42] << 8;
            secsSince1900 |= (unsigned long)flinkPacketBuffer[43];
            return secsSince1900 - 2208988800UL + timeZone * SECS_PER_HOUR;
        }
    }
    #ifdef TIME_DEBUG
    Serial.println("[NTP] No NTP response");
    #endif
    return 0; // return 0 if unable to get the time
}

// send an NTP request to the time server at the given address
void FlinkTime::sendNtpPacket(IPAddress &address) {
    // set all bytes in the buffer to 0
    memset(flinkPacketBuffer, 0, NTP_PACKET_SIZE);
    // Initialize values needed to form NTP request
    // (see URL above for details on the packets)
    flinkPacketBuffer[0] = 0b11100011;   // LI, Version, Mode
    flinkPacketBuffer[1] = 0;     // Stratum, or type of clock
    flinkPacketBuffer[2] = 6;     // Polling Interval
    flinkPacketBuffer[3] = 0xEC;  // Peer Clock Precision
    // 8 bytes of zero for Root Delay & Root Dispersion
    flinkPacketBuffer[12] = 49;
    flinkPacketBuffer[13] = 0x4E;
    flinkPacketBuffer[14] = 49;
    flinkPacketBuffer[15] = 52;
    // all NTP fields have been given values, now
    // you can send a packet requesting a timestamp:
    flinkUdp.beginPacket(address, 123); //NTP requests are to port 123
    flinkUdp.write(flinkPacketBuffer, NTP_PACKET_SIZE);
    flinkUdp.endPacket();
}

 FlinkTime flinkClock;