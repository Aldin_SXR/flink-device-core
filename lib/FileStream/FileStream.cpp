/**
 * @file FileStream.h
 * @date 10.12.2017
 * @author Aldin Kovačević
 * 
 * SPIFFS read/write library synthesized from the original functions
 * 
 */

#include "FileStream.h"

FileStream::FileStream() {}
FileStream::~FileStream() {}

bool FileStream::write(String name, String content) {
  SPIFFS.begin();
  content += '*';
  File file = SPIFFS.open(name.c_str(), "w");
  //verify the the file was  opened:
  if (!file) {
    #ifdef FILE_DEBUG
    String errorMessage = "Can't open '" + name + "' !\r\n";
    Serial.println(errorMessage);
    Serial.println();
    #endif
    return false;
  } else {
    file.write((uint8_t *)content.c_str(), content.length());
    file.close();
    SPIFFS.end();
    return true;
  }
}

String FileStream::read(String name) {
  SPIFFS.begin();
  String contents;
  File file = SPIFFS.open(name.c_str(), "r");
  if (!file) {
    #ifdef FILE_DEBUG
    String errorMessage = "Can't open '" + name + "' !\r\n";
    Serial.println(errorMessage);
    #endif
    SPIFFS.end();
    return "ERROR";
  }
  else {
    String s = file.readStringUntil('*');
    contents = s;
    file.close();
    SPIFFS.end();
    return contents;
  }    
}

bool FileStream::exists(String file) {
  bool e = false;
  SPIFFS.begin();
  if (SPIFFS.exists(file))
    e = true;
  SPIFFS.end();
  return e;
}

void FileStream::remove(String file) {
    SPIFFS.begin();
    SPIFFS.remove(file);
    SPIFFS.end();
}

FileStream fileStream;