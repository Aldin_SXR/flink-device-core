/**
 * @file FileStream.h
 * @date 10.12.2017
 * @author Aldin Kovačević
 * 
 * SPIFFS read/write library synthesized from the original functions
 * 
 */

#ifndef FILESTREAM_H
#define FILESTREAM_H

#include "FS.h"
#include "WString.h"
#define FILE_DEBUG

class FileStream {
    public:
        FileStream();
        ~FileStream();
        bool write(String name, String content);
        bool exists(String file);
        String read(String name);
        void remove(String file);
};

extern FileStream fileStream;

#endif