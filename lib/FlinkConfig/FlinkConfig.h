/**
 * 
 * @file Config.h
 * @date 10.12.2017
 * @author Aldin Kovačević
 *
 *  Config file with constant data for the OTA update (use your own configurations)
 */

#ifndef FLINK_CONFIG_H
#define FLINK_CONFIG_H

#define HOST "139.59.136.102"
#define PORT 80
#define URI "/new/otaupdate.php"
#define DEVICE_TYPE "flink-cool"
#define CURRENT_VERSION "cool-ota-v0.1"

#endif 