#include "Device.h"

void setup() {
    Serial.begin(115200);
    delay(10);

    /* Initial configuration */
    configServer();
    configDevice();
    configMqtt();
}

void loop() {
    s_wifiConnection.execute();
    server.handleClient();
    flinkClock.loop();
    reset();
}