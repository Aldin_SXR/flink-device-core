/* POST message from the device containing the required information for successful device connection to MQTT */
void postUUID() {
    t_setDefault.disable();
    StaticJsonBuffer<220> jsonBuffer;
    JsonObject & receivedPayload = jsonBuffer.parseObject(server.getMessage());
    String userName = receivedPayload["Username"];
    String password = receivedPayload["Password"];
    String uuid = receivedPayload["Uuid"];
    uuid.replace("-", "");

    /* 1 at byte 200 symbolizes the received inital POST message, and enables the device
    to decide whether to setup a new MQTT connection or connect with existing credentials */
    eeprom.writeByte(200, 1);

    fileStream.write("uuid", uuid);
    fileStream.write("userName", userName);
    fileStream.write("password", password);

    t_mqttReconnect.enableIfNot();
    t_setDefault.enableDelayed(20000);
	#ifdef DEBUG
    Serial.println("[HTTP] Payload: " + server.getMessage());
    Serial.println("[HTTP] Username: " + userName + "; Password: " + password + "; UUID: " + uuid);
	#endif
}

/* Returns the device to SmartConfig */
void returnToSmartConfig() {
    enableSmartConfig();
}

/* Returns device type to calling application */
void getConfirm() {
    String deviceType = "Cool"; // name/type of device
	#ifdef DEBUG
    Serial.println("[GET] getConfirm() message received");
    Serial.println(deviceType);
	#endif
    server.send(200, "text/plain", deviceType);
    server.handleClient();
}