/* Configure the WiFi connection and SSID/paswword */
void configDevice() {
        WiFi.mode(WIFI_STA);
        String mac = WiFi.macAddress();
        mac.replace(":", "");
        mac.toLowerCase();
		#ifdef DEBUG
        Serial.println("[Config] MAC Address: " + mac);
		#endif
        fileStream.write("mac", mac);
        delay(500);
        WiFi.disconnect();

        String ssid;
        String pass;

        /* 1 at byte 150 symbolizes that a SPIFFS update had occured, and main credentials had been saved
        in EEPROM, where they will be retrieved from */
        if (eeprom.readByte(150) == 1) {
            ssid = eeprom.readString(30, 29);
            pass = eeprom.readString(60, 29);
            String uuid = eeprom.readString(90, 40);
			#ifdef DEBUG
            Serial.println("ssid: " + ssid);
            Serial.println("pass: " + pass);
            Serial.println("uuid: " + uuid);
			#endif

            fileStream.write("ssid", ssid);
            fileStream.write("pass", pass);
            fileStream.write("uuid", uuid);
            t_mqttReconnect.enableIfNot();
            if (eeprom.readByte(400) == 0)
                eeprom.writeByte(150, 0); // SPIFFS update successfully handled
        } else {
            ssid = fileStream.read("ssid");
            pass = fileStream.read("pass");
        }

        #ifdef DEBUG
        Serial.println("\n\n");
        Serial.println("[Config] Stored SSID: " + (String) ssid);
        Serial.println("[Config] Stored PASS: " + (String) pass);
        Serial.println("");
		#endif

        t_checkStatusWifiConnection.enableIfNot();
    }

/* Configure the ESP8266 server */
void configServer() {
    //start the server and define routes it will use
    server.begin();
    server.on("/config", postUUID);
    server.on("/getConfirm", getConfirm);
    server.on("/returnToSmartConfig", returnToSmartConfig);
}

/* Configure the MQTT client */
void configMqtt() {
    client.setServer(MQTTServer, 1883);
    client.setMaxTopicLength(256);
    client.setCleanSession(true);
    client.onConnect(onMqttConnect);
    client.onMessage(callback);
    //  client.setKeepAlive(5);
}

/* Return the device to default settings if no MQTT connection is made */
void setDefault() {
    t_setDefault.disable();
    WiFi.disconnect();
    actualReset();
}

/* Perform the Over-the-Air (OTA) code update */
void OTAupdate() {
    t_OTAupdate.disable();
    if ((WiFi.status() == WL_CONNECTED)) {
		#ifdef DEBUG
        Serial.println("[OTA] Checking for code updates...");
		#endif

        flinkHttpUpdate.otaUpdate();
    }
}

/* Perform the Over-the-Air (OTA) SPIFFS update */
void OTASpiffsUpdate() {
    t_OTASpiffsUpdate.disable();
    if ((WiFi.status() == WL_CONNECTED)) {
		#ifdef DEBUG
        Serial.println("[OTA] Checking for SPIFFS updates...");
		#endif

        eeprom.writeByte(150, 1); // set the SPIFFS update flag 
        if (eeprom.readByte(400) != 1)
            eeprom.writeByte(400, 1); // SPIFFS update started flag
        String ssid = fileStream.read("ssid");
        String pass = fileStream.read("pass");
        String uuid = fileStream.read("uuid");

        eeprom.writeString(30, ssid);
        eeprom.writeString(60, pass);
        eeprom.writeString(90, uuid);
        
        flinkHttpUpdate.otaSpiffsUpdate();
    }
}

/* Resync time when the time zone is changed */
void resyncTime() {
    t_resyncTime.disable();
    String tZone = fileStream.read("timezone");
    flinkClock.timeSync(tZone.toInt());
}