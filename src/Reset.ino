unsigned long keyPrevMillis = 0;
byte holdButtonCount = 0;
byte prevKeyState = HIGH;

/* called when key goes from pressed to not pressed */
void keyRelease() {
    byte holdButtonCountMax = 5;
    if (holdButtonCount >= holdButtonCountMax) {
        actualReset();
    } else {
        t_checkStatusWifiConnection.enableIfNot();
        holdButtonCount = 0;
    }
}

void actualReset() {
    String uuid = fileStream.read("uuid");
    String mac = fileStream.read("mac");
    String resetTopic = uuid + "/" + mac;
    String message = "{\"status\":\"reset\"}";
    client.publish(resetTopic.c_str(), 0, true, message.c_str());

    fileStream.remove("ssid");
    fileStream.remove("pass");

    eeprom.writeByte(200, 0);
    for (int i = 29; i < 149; i++) {
        eeprom.writeByte(i, 0);
    }

    WiFi.disconnect();
    s_wifiConnection.disableAll();
    t_checkStatusWifiConnection.enableIfNot();

    # ifdef DEBUG
    Serial.println(F("[RESET] Reset performed."));
    #endif
    ESP.restart();
}

void reset() {
    const unsigned long interval = 1000;
    if (millis() - keyPrevMillis >= interval) {
        keyPrevMillis = millis();
        byte currKeyState = digitalRead(2);
        if ((prevKeyState == LOW) && (currKeyState == HIGH)) {
            keyRelease();
        } else if (currKeyState == LOW) {
            holdButtonCount++;
            #ifdef DEBUG
            Serial.println(holdButtonCount);
            #endif
            s_wifiConnection.disableAll();
        }
        prevKeyState = currKeyState;
    }
}