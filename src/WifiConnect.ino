/* Attempts to connect to WiFi with given credentials */
void wifiConnect() {
	#ifdef DEBUG
    Serial.println("[WiFi] Trying to connect to WiFi...");
	#endif
    t_mqttReconnect.disable();
    t_checkStatusWifiConnection.disable();

    /* wait for connection results */
    if (WiFi.isConnected()) {
        t_checkStatusWifiConnection.enableIfNot();
        WiFi.setAutoReconnect(true);
        WiFi.setAutoConnect(true);
        t_wifiConnect.disable();
		#ifdef DEBUG
        Serial.println("[WiFi] WiFi is connected.");
        Serial.println("[WiFi] SSID: " + WiFi.SSID());
        Serial.println("[WiFi] PASS: " + WiFi.psk());
        Serial.print("[WiFi] IP address: " + WiFi.localIP().toString());
        Serial.println();
		#endif
        
        /* check for unsuccessful SPIFFS update and resume */
        if (eeprom.readByte(400) == 1) {
            t_OTASpiffsUpdate.enableIfNot();
        }
        /* start the clock synchronization */
        if (fileStream.exists("timezone")) {
            String tZone = fileStream.read("timezone");
            flinkClock.timeSync(tZone.toInt());
        } else {
            flinkClock.timeSync(1); // default to 1 (CET)
        }
    } else {
         #ifdef DEBUG
        Serial.println("[WiFi] Still not connected to WiFi.");
        #endif
        
        /* check if the "pass" (password) file exists => failure means the device isn't set up, so it goes to SmartConfig */
        if (!fileStream.exists("pass")) {
            t_enableSmartConfig.enableIfNot();
        } else {
            String ssid = fileStream.read("ssid");
            String pass = fileStream.read("pass");
            yield();
            WiFi.begin(ssid.c_str(), pass.c_str());
        }
    }
}

/* Starts the actual SmartConfig (device adding) procedure */
void enableSmartConfig() {
	#ifdef DEBUG
    Serial.println("[SmartConfig] Smart Config started...");
	#endif
    t_checkStatusWifiConnection.disable();
    WiFi.stopSmartConfig();
    WiFi.disconnect();
    yield();

    WiFi.beginSmartConfig();
    t_enableSmartConfig.disable();
    t_checkStatusSmartConfig.enableIfNot();
    t_wifiConnect.disable();
	#ifdef DEBUG
    Serial.println("[SmartConfig] Smart Config enabled.");
	#endif
}

/* Checks the current status of device connection to WiFi and starts the entire connection process */
void checkStatusWifiConnection() {
	#ifdef DEBUG
    Serial.println("[WiFi] Checking WiFi connection status... OK");
	#endif
    if (!WiFi.isConnected()) { // if device isn't connected, call connection function wifiConnect()
        t_mqttReconnect.disable();
		#ifdef DEBUG
        Serial.println("[WiFi] Retrying WiFi connection...");
		#endif
        t_wifiConnect.enableIfNot();
    } else { // if device is connected to WiFi, but not MQTT, start the MQTT connection
        if (!client.connected()) {
            if (eeprom.readByte(200) == 1) {
                t_mqttReconnect.enableIfNot();
            }
        }
    }
}

/* Waits for the status and result of SmartConfig */
void checkStatusSmartConfig() {
	#ifdef DEBUG
    Serial.println("[SmartConfig] Waiting for Smart Config...");
	#endif
    /* once the SmartConfig procedure is done, write the network details to SPIFFS */
    if (WiFi.smartConfigDone()) {
        yield();
        WiFi.waitForConnectResult();
        yield();
        if (WiFi.isConnected()) { // successful connection
            t_checkStatusSmartConfig.disable();

            /* save SSID and password to SPIFFS upon successful connection */
            fileStream.write("ssid", WiFi.SSID());
            fileStream.write("pass", WiFi.psk());
            fileStream.write("local_ip", WiFi.localIP().toString());

            t_disableSmartConfig.disable();
            WiFi.setAutoReconnect(true);
            WiFi.setAutoConnect(true);
            t_setDefault.enableDelayed(120000); // if no POST message for MQTT, device returns to SmartConfig in 2 minutes

            #ifdef DEBUG
            Serial.println("[SmartConfig] Smart Config done; successful connection.");
            Serial.println("[WiFi] IP address: " + WiFi.localIP().toString());
            /* start the clock synchronization */
            flinkClock.timeSync(1); // default to 1 (CET)
            t_checkStatusWifiConnection.enableIfNot();
			#endif
        } else { // unsuccessful connection
			#ifdef DEBUG
            Serial.println("[SmartConfig] Smart Config done; unsuccessful connection.");
			#endif
            t_disableSmartConfig.enableIfNot();
            t_enableSmartConfig.enableIfNot();
        }
    }
}

/* Disabling SmartConfig */
void disableSmartConfig() {
	#ifdef DEBUG
    Serial.println("[SmartConfig] Disabling Smart Config...");
	#endif
    WiFi.stopSmartConfig();
	#ifdef DEBUG
    Serial.println(F("[SmartConfig] Smart Config disabled."));
	#endif
    t_disableSmartConfig.disable();
}