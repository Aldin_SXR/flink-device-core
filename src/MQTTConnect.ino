/* Starting the MQTT connection procedure, with required session details */
void mqttReconnect() {
    espClient.stop();
    if (WiFi.isConnected()) {
        client.disconnect(); 
        yield();
        String uuid = fileStream.read("uuid");
        String mac = fileStream.read("mac");

        /* Setting the username and password */
        # ifdef DEBUG
        Serial.println(F("[MQTTT] Trying to connect..."));
		#endif
        const char * userName = "flink-cool";
        const char * password = "eDLftWUywHYmc3d8";
        client.setCredentials(userName, password);

        /* Setting up LWT (Last Will & Testament) */
        String willTopic = uuid + "/" + mac;
        char willT[willTopic.length() + 1];
        size_t destinationSize = sizeof(willT);
        strncpy(willT, willTopic.c_str(), destinationSize);

        const char* willMessage = "{\"status\":\"disconnnected\"}";
		#ifdef DEBUG
        Serial.print("[Heap] Free heap: ");
        Serial.println(ESP.getFreeHeap());
		#endif
        client.setWill(willT, 0, true, willMessage);

        /* Attempting connection */
        #ifdef DEBUG
        Serial.println("[MQTT] About to enter client.connect()");
		#endif
        client.connect();
        yield();
    }
}

/* MQTT on-connection callback function */
void onMqttConnect(bool sessionPresent) {
	# ifdef DEBUG
    Serial.println("\n<--- [AsyncMQTT] Connected to MQTT. --->");
    Serial.print("[MQTT] Session present: ");
    Serial.println(sessionPresent);
	#endif
    String uuid = fileStream.read("uuid");
    String mac = fileStream.read("mac");
    if (client.connected()) { // if client connects, stop unnecessary tasks and subscribe/publish to required topics
        t_setDefault.disable();
        t_mqttReconnect.disable();
		#ifdef DEBUG
        Serial.print("[Heap] Free heap: ");
        Serial.println(ESP.getFreeHeap());
		#endif

        /* Topic-i za subscribe/publish */
        String subscribeTopic = mac + "/" + uuid + "/+";
        String publishTopic = uuid + "/" + mac + "/ok";
        client.subscribe(subscribeTopic.c_str(), 0);
        client.publish(publishTopic.c_str(), 0, true, "Hello. Successfully connected.");
        String willTopic = uuid + "/" + mac;
        client.publish(willTopic.c_str(), 0, true, "{\"status\":\"connected\"}");

		#ifdef DEBUG
        Serial.print("[Heap] Free heap: ");
        Serial.println(ESP.getFreeHeap());
		#endif

        #ifdef DEBUG
        Serial.print(F("[MQTT] Update topic: "));
        Serial.println(mac + "/" + uuid + "/update");
		#endif

        t_checkStatusWifiConnection.enableIfNot();
    }
}

/* MQTT message-received callback function */
void callback(char * topic, char * payload, AsyncMqttClientMessageProperties properties, size_t length, size_t index, size_t total) {
    StaticJsonBuffer < 500 > jsonBuffer;
    String mqttTopic = topic;
    String data;
    for (int i = 0; i < length; i++) {
        data += (char) payload[i];
    }

    #ifdef DEBUG
    Serial.println(F("-----------------------------------------------------------------------------"));
    Serial.println(F("|                      Function MQTTmessage() called.                        |"));
    Serial.println(F("|                                                                            |"));
    Serial.println(F("| Data :                ** Arrived **                                        |"));
    Serial.print(F("| Topic :               ** "));
    Serial.print(mqttTopic);
    Serial.println(F(" **"));
    Serial.print(F("| Message :             ** "));
    Serial.print(data);
    Serial.println(F(" **"));
    Serial.println(F("-----------------------------------------------------------------------------"));
    Serial.println();
	#endif

    JsonObject & root = jsonBuffer.parseObject(data);
    String f_type = root["type"];
    String f_brand = root["brand"];
    String f_device = root["device"];
    String f_model = root["model"];
    String f_mode = root["mode"];
    String f_temperature = root["temperature"];
    String f_fan = root["fan"];
    String f_onOff = root["on/off"];
    String f_timeZone = root["timezone"];
    String f_status = root["status"];
    String f_auto = root["auto"];

    String uuid = fileStream.read("uuid");
    String mac = fileStream.read("mac");

    if (mqttTopic == (mac + "/" + uuid + "/ok")) {
        t_setDefault.disable();
		#ifdef DEBUG
        Serial.println("[MQTT] Received success topic.");
		#endif
    }

    /* OTA update MQTT messages; code vs SPIFFS update */
    if (mqttTopic == (mac + "/" + uuid + "/update")) {
        if (f_type == "code")
            t_OTAupdate.enableDelayed(10);
        if (f_type == "spiffs")
            t_OTASpiffsUpdate.enableDelayed(10);
    }

    /* Real-time clock settings MQTT messages */
    if (mqttTopic == (mac + "/" + uuid + "/time")) {
        if (f_timeZone != "") {
            fileStream.write("timezone", f_timeZone);
            t_resyncTime.enableDelayed(10);
        } else
            flinkClock.digitalClockDisplay();
    }
}